class GroovyTut{
  static void main(String[] args){
    println("Hello World");

    def age = "Dog";
    age = 40;

//see below for math stuff. See that everything follows the same pattern, except for division
//addition with and without decimals
    println("5 + 4 = " + (5 + 4));
    println("5.2 + 4.4 = " + (5.2.plus(4.4)));
//subtraction
    println("5 - 4 = " + (5 - 4));
    println("5.2 - 4.4 = " + (5.2.minus(4.4)));
//division
    println("5 / 4 = " + (5.intdiv(4)));
    println("5.2 / 4.4 = " + (5 / 4));
//multiplication
    println("5 * 4 = " + (5 * 4));
    println("5.2 * 4.4 = " + (5.2.multiply(4.4)));

//largest and smallest values
    println("Biggest Int " + Integer.MAX_VALUE);
    println("Smallest Int " + Integer.MIN_VALUE);\

    println("Biggest Float " + Float.MAX_VALUE);
    println("Smallest Float " + Float.MIN_VALUE);

    println("Biggest Double " + Double.MAX_VALUE);
    println("Smallest Double " + Double.MIN_VALUE);

    def randNum = 2.0;
    println("Math.abs(-2.45) = " + (Math.abs(-2.45)));
//not only do we have Math.abs for absolute value, but tons of other ones
    println(Math.round(2.45));
    println(3.0.equals(2.0));
    println(Math.sqrt(2.45));
    println(Math.cbrt(2.45));
    println(Math.ceil(2.45));
    println(Math.ceil(2.45));
    println(Math.floor(2.45));
    println(Math.min(2,3));
    println(Math.max(2,3));

  }
}
